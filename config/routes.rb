Rails.application.routes.draw do
	root to: 'teams#index'
  devise_for :users
  get '/drivers' => 'drivers#index'

  get '/drivers/show/:id' => 'drivers#show'

  get 'drivers/new'

  get 'drivers/create'

  get 'drivers/edit'

  get 'drivers/update'

  get '/drivers/delete/:id' => 'drivers#delete'

  get 'drivers/destroy'

  get '/teams' => 'teams#index'

  get '/teams/show/:id' => 'teams#show'

  get 'teams/new'

  get 'teams/create'

  get 'teams/edit'

  get 'teams/update'

  get '/teams/delete/:id' => 'teams#delete'

  get 'teams/destroy'
	resources :teams
	resources :drivers

	get 'signup'  => 'users#new'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
