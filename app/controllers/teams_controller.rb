class TeamsController < ApplicationController
  def index
	@teams = Team.all
  end

  def show
	@team = Team.find(params[:id])
	@drivers = @team.drivers
  end

  def new
	@team = Team.new
  end

  def create
	@team = Team.new(team_params)
	if @team.save
	redirect_to(:action => 'index')
	else
	render('new')
	end
  end

  def edit
	@team = Team.find(params[:id])
  end

  def update
	@team = Team.find(params[:id])
	if @team.update_attributes(team_params)
	redirect_to(:action => 'show', :id => @team.id)
	else
	render('index')
	end
  end

  def delete
	@team = Team.find(params[:id])
  end

  def destroy
	Team.find(params[:id]).destroy
	redirect_to(:action => 'index')
  end

	private
	def team_params
	params.require(:team).permit(:name, :image, :description, :shop)
	end
end
