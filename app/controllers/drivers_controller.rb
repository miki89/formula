class DriversController < ApplicationController
  def index
	@drivers = Driver.all
  end

  def show
	@driver = Driver.find(params[:id])
  end

  def new
	@driver = Driver.new
  end

  def create
	@driver = Driver.new(driver_params)
	if @driver.save
	redirect_to(:action => 'index')
	else
	render('new')
	end
  end

  def edit
	@driver = Driver.find(params[:id])
  end

  def update
	@driver = Driver.finf(params[:id])
	if @driver.update_attributes(driver_params)
	redirect_to(:action => 'show', :id => driver.id)
	else
	render('index')
	end
  end

  def delete
	@driver = Driver.find(params[:id])
  end

  def destroy
	Driver.find(params[:id]).destroy
	redirect_to(:action => 'index')
  end

	private
	def driver_params
	params.require(:driver).permit(:name, :image, :bio, :shop)
	end
end
