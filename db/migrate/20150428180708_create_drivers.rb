class CreateDrivers < ActiveRecord::Migration
  def change
    create_table :drivers do |t|
	t.string :name
	t.string :image
	t.string :bio
	t.string :shop
	t.references :team
      t.timestamps null: false
    end
  end
end
